﻿using UnityEngine;

/// <summary>
/// List of GameTags defined
/// </summary>
public static class GameTags
{
    public const string ObstacleResetTrigger = "ObstacleResetTrigger";
    public const string UI = "UI";
    public const string VlappyMan = "VlappyMan";
}
