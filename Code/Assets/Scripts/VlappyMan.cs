﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the Vlappy Man character
/// </summary>
public class VlappyMan : MonoBehaviour
{
    /// <summary>
    /// Are we usign VVVVVV or real world style gravity
    /// </summary>
    private bool isVVVVVVGravity;

    /// <summary>
    /// Static gravity to apply to the character for VVVVVV mode
    /// </summary>
    private static float VVVVVVGravityFactor = 4.0f;

    /// <summary>
    /// Static gravity to apply to the character for Real World mode
    /// </summary>
    private static float RLGravityFactor = 1.0f;

    /// <summary>
    /// Current velocity of the player
    /// </summary>
    private Vector2 velocity;

    /// <summary>
    /// Access the the PointsUI script
    /// </summary>
    private PointsText pointsUI;

    /// <summary>
    /// Starts this instance.
    /// </summary>
	private void Start()
    {
        isVVVVVVGravity = SaveManager.GetGravity();

        if (!isVVVVVVGravity)
        {
            rigidbody2D.gravityScale = RLGravityFactor;
        }
        else
        {
            velocity = new Vector2(0, VVVVVVGravityFactor * -1);
        }

        pointsUI = GameObject.FindGameObjectWithTag(GameTags.UI).GetComponentInChildren<PointsText>();

        SoundController.Instance.InitializeSound("Jump1", false);
        SoundController.Instance.SetVolume("Jump1", 0.7f);
	}
	
    /// <summary>
    /// Updates this instance.
    /// </summary>
	private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !UnityHelper.IsGamePaused())
        {
            FlipGravity();
            FlipSprite();
            SoundController.Instance.PlaySoundEffect("Jump1");
        }

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                FlipGravity();
                FlipSprite();
                SoundController.Instance.PlaySoundEffect("Jump1");
            }
        }
	}

    /// <summary>
    /// Fixed Update this instance.
    /// </summary>
    private void FixedUpdate()
    {
        if (isVVVVVVGravity)
        {
            rigidbody2D.velocity = velocity;
        }
    }

    /********************************************************************************/

    /// <summary>
    /// Handle trigger collision resolution for Vlappy Man
    /// </summary>
    /// <param name="collision">Object collided with</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Handle collision with obstacle
        ObstaclePart obstacle = collision.gameObject.GetComponent<ObstaclePart>();
        if (obstacle != null)
        {
            SaveManager.AddScore(pointsUI.Points);
            SaveManager.IncrementDeaths();

            UnityHelper.PauseGame(true);
            Application.LoadLevelAdditive("DeathScreen");
        }
    }

    /// <summary>
    /// Flips the sprite of Vlappy Man
    /// </summary>
    private void FlipSprite()
    {
        Vector3 scale = transform.localScale;
        scale.y *= -1;
        transform.localScale = scale;
    }

    /// <summary>
    /// Flips the gravity depending on the method of gravity used
    /// </summary>
    private void FlipGravity()
    {
        if (!isVVVVVVGravity)
        {
            rigidbody2D.gravityScale *= -1.0f;
        }
        else
        {
            velocity *= -1.0f;
        }
    }
}
