﻿using UnityEngine;
using System.Collections;

public class DeathText : MonoBehaviour
{
    private void Start()
    {
        int points = SaveManager.GetDeaths();
        guiText.text = ""+points;
    }
}
