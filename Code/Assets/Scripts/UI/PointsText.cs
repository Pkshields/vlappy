﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the very basic PointsText functionality
/// </summary>
public class PointsText : MonoBehaviour
{
    /// <summary>
    /// Points the current player has
    /// </summary>
    /// <value>
    /// The points.
    /// </value>
    public int Points
    {
        get;
        set;
    }

    /// <summary>
    /// Starts this instance.
    /// </summary>
    private void Start()
    {
        Points = 0;
        guiText.text = ""+Points;
    }

    /// <summary>
    /// Increments the points total
    /// </summary>
	public void Increment()
    {
        Points++;
        guiText.text = "" + Points;
    }
}
