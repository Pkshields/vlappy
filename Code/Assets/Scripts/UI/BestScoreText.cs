﻿using UnityEngine;
using System.Collections;

public class BestScoreText : MonoBehaviour
{
    private void Start()
    {
        int points = SaveManager.GetTopScore();
        guiText.text = ""+points;
    }
}
