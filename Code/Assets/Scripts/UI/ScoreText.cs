﻿using UnityEngine;
using System.Collections;

public class ScoreText : MonoBehaviour
{
    private void Start()
    {
        int points = SaveManager.GetLastScore();
        guiText.text = ""+points;
    }
}
