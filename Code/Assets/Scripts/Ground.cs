﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the Ground object
/// </summary>
public class Ground : MonoBehaviour
{
    /// <summary>
    /// Starts this instance.
    /// </summary>
	private void Start ()
    {
        GetComponent<SpriteRenderer>().color = ColorPalette.GetCurrentPrimaryColor();
	}
}
