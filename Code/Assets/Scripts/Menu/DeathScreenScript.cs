﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the Main Menu functionality
/// </summary>
public class DeathScreenScript : MonoBehaviour
{
    /// <summary>
    /// The skin to use on the menu items
    /// </summary>
    public GUISkin currentSkin;

    /// <summary>
    /// Updates this instance.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UnityHelper.PauseGame(false);
            Application.LoadLevel("Game");
        }
    }

    /// <summary>
    /// Draws and handles GUI the only way Unity knows how
    /// </summary>
    private void OnGUI()
    {
        const int buttonWidth = 184;
        const int buttonHeight = 30;

        GUI.skin = currentSkin;

        int centerWidth = Screen.width / 2 - buttonWidth / 2;
        int centerHeight = Screen.height / 2 - buttonHeight / 2;

        if (GUI.Button(
                new Rect(centerWidth,
                        centerHeight + 80,
                        buttonWidth,
                        buttonHeight), 
                "Retry"))
        {
            UnityHelper.PauseGame(false);
            Application.LoadLevel("Game");
        }

        if (GUI.Button(
                new Rect(centerWidth,
                        centerHeight + 120,
                        buttonWidth,
                        buttonHeight), 
                "Menu"))
        {
            UnityHelper.PauseGame(false);
            Application.LoadLevel("MainMenu");
        }
    }
}
