﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the Main Menu functionality
/// </summary>
public class MainMenuScript : MonoBehaviour
{
    /// <summary>
    /// The skin to use on the menu items
    /// </summary>
    public GUISkin currentSkin;

    private bool isVVVVVVMode;

    private bool isPlayingSong1 = true;

    /// <summary>
    /// Starts this instance.
    /// </summary>
    private void Start()
    {
        SoundController.Instance.PlaySong("Background1");
        SoundController.Instance.InitializeSound("Button1", false);
        SoundController.Instance.SetVolume("Button1", 0.7f);
        isVVVVVVMode = SaveManager.GetGravity();
    }

    /// <summary>
    /// Updates this instance.
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SaveManager.SetGravity(isVVVVVVMode);
            Application.LoadLevel("Game");
        }
    }

    /// <summary>
    /// Draws and handles GUI the inly way Unity knows how
    /// </summary>
    private void OnGUI()
    {
        const int buttonWidth = 480;
        const int buttonHeight = 30;

        int centerWidth = Screen.width / 2 - buttonWidth / 2;
        int centerHeight = Screen.height / 2 - buttonHeight / 2;

        GUI.skin = currentSkin;

        if (GUI.Button(
                new Rect(centerWidth,
                         centerHeight + 30,
                         buttonWidth,
                         buttonHeight), 
                "Start Game"))
        {
            SoundController.Instance.PlaySoundEffect("Button1");
            SaveManager.SetGravity(isVVVVVVMode);
            Application.LoadLevel("Game");
        }

        if (GUI.Button(
                new Rect(centerWidth,
                         centerHeight + 110,
                         buttonWidth,
                         buttonHeight), 
                (isVVVVVVMode ? "VVVVVV Mode" : "Gravity Mode")))
        {
            SoundController.Instance.PlaySoundEffect("Button1");
            isVVVVVVMode = !isVVVVVVMode;
        }

        if (GUI.Button(
                new Rect(centerWidth,
                         centerHeight + 140,
                         buttonWidth,
                         buttonHeight), 
                (isPlayingSong1 ? "I really like this song..." : "... But this song might be better!")))
        {
            SoundController.Instance.PlaySoundEffect("Button1");
            isPlayingSong1 = !isPlayingSong1;

            if (isPlayingSong1)
            {
                SoundController.Instance.PlaySong("Background1");
            }
            else
            {
                SoundController.Instance.PlaySong("Background2");
            }
        }

        if (GUI.Button(
                new Rect(centerWidth + 250,
                         centerHeight + 210,
                         buttonWidth,
                         buttonHeight),
                (!SoundController.Instance.IsMutedAll() ? "Mute" : "Unmute")))
        {
            SoundController.Instance.MuteAll(!SoundController.Instance.IsMutedAll());
        }
    }
}
