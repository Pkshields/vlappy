﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple moving script to move things by a fixed velocity
/// </summary>
public class MovingScript : MonoBehaviour {

    /// <summary>
    /// Velocity to move 
    /// </summary>
    public Vector2 velocity = Vector2.zero;

    /// <summary>
    /// Set the velocity!
    /// </summary>
	void Start() {
        rigidbody2D.velocity = velocity;
	}
}
