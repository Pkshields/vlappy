﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the each separate Obstacle part
/// </summary>
public class ObstaclePart : MonoBehaviour {

    /// <summary>
    /// The initial Y position
    /// </summary>
    private float startingYPosition;

    /// <summary>
    /// Starts the manual.
    /// </summary>
    public void StartManual()
    {
        startingYPosition = transform.position.y;
	}
	
    /// <summary>
    /// Updates this instance.
    /// </summary>
	void Update () {
	}

    /// <summary>
    /// Sets the Y position
    /// </summary>
    /// <param name="yPosition">The Y position</param>
    public void SetYPosition(float yPosition)
    {
        Vector3 position = transform.position;
        position.y = startingYPosition - yPosition;
        transform.position = position;
    }

    public void SetColor(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }
}
