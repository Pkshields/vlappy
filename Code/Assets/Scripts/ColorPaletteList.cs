﻿using UnityEngine;

/// <summary>
/// Predetermined list of color palettes
/// </summary>
class ColorPaletteList
{
    /// <summary>
    /// The list of possigle color palettes
    /// </summary>
    public static ColorPaletteData[] List = new ColorPaletteData[] 
    {
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(81, 7, 115), UnityHelper.GenerateUnityRGB(245, 135, 236)),   // Purple
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(189, 176, 0), UnityHelper.GenerateUnityRGB(245, 229, 10)),   // Yellow
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(0, 107, 63), UnityHelper.GenerateUnityRGB(180, 10, 10)),     // Green/Red
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(163, 103, 0), UnityHelper.GenerateUnityRGB(255, 245, 28)),   // Orange
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(150, 32, 32), UnityHelper.GenerateUnityRGB(191, 94, 183)),   // Red/Pink
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(74, 74, 74), UnityHelper.GenerateUnityRGB(170, 170, 170)),   // Grey
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(29, 79, 29), UnityHelper.GenerateUnityRGB(57, 163, 57)),     // Green
        new ColorPaletteData(UnityHelper.GenerateUnityRGB(30, 44, 99), UnityHelper.GenerateUnityRGB(52, 76, 173)),     // Blue
    };

    /// <summary>
    /// Gets the number of possible color palettes
    /// </summary>
    /// <value>
    /// The number of possible color palettes
    /// </value>
    public static int Count
    {
        get { return List.Length; }
    }
}

/// <summary>
/// Struct to hold the color palette data
/// </summary>
public struct ColorPaletteData
{
    /// <summary>
    /// Primary color
    /// </summary>
    /// <value>
    /// Primary color
    /// </value>
    public Color PrimaryColor
    {
        get;
        set;
    }

    /// <summary>
    /// Secondary color
    /// </summary>
    /// <value>
    /// Secondary color
    /// </value>
    public Color SecondaryColor
    {
        get;
        set;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ColorPaletteData"/> struct.
    /// </summary>
    /// <param name="primaryColor">Primary color</param>
    /// <param name="secondaryColor">Secondary color</param>
    public ColorPaletteData(Color primaryColor, Color secondaryColor)
    {
        this.PrimaryColor = primaryColor;
        this.SecondaryColor = secondaryColor;
    }
}