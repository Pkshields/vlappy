﻿using UnityEngine;

/// <summary>
/// Helper methods related to the camera
/// </summary>
public static class CameraHelper
{
    /// <summary>
    /// Gets the bounds of the camera 
    /// </summary>
    /// <returns>The bounds of the camera</returns>
    public static CameraBounds GetCameraBounds()
    {
        Camera cam = Camera.main;
		float halfHeight = cam.orthographicSize;
        float halfWidth = ((halfHeight * 2.0f) * cam.aspect) / 2.0f; // width = height * aspect
        Vector3 position = cam.transform.position;

        return new CameraBounds(position.x - halfWidth,
                               position.x + halfWidth,
                               position.y - halfHeight,
                               position.y + halfHeight);
    }

    /// <summary>
    /// Gets the widtha dn height of the camera
    /// </summary>
    /// <returns>Camera dimensions</returns>
    public static Vector2 GetCameraDimensions()
    {
        Camera cam = Camera.main;
        float height = cam.orthographicSize * 2;
        float width = height * cam.aspect;
        
        return new Vector2(width, height);
    }
}

/// <summary>
/// Holds the data related to the bounds of the camera
/// </summary>
public struct CameraBounds
{
    /// <summary>
    /// Gets the left edge.
    /// </summary>
    /// <value>
    /// The left edge.
    /// </value>
    public float LeftEdge
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the right edge.
    /// </summary>
    /// <value>
    /// The right edge.
    /// </value>
    public float RightEdge
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the top edge.
    /// </summary>
    /// <value>
    /// The top edge.
    /// </value>
    public float TopEdge
    {
        get;
        private set;
    }

    /// <summary>
    /// Gets the bottom edge.
    /// </summary>
    /// <value>
    /// The bottom edge.
    /// </value>
    public float BottomEdge
    {
        get;
        private set;
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="CameraBounds"/> struct.
    /// </summary>
    /// <param name="leftEdge">The left edge.</param>
    /// <param name="rightEdge">The right edge.</param>
    /// <param name="topEdge">The top edge.</param>
    /// <param name="bottomEdge">The bottom edge.</param>
    public CameraBounds(float leftEdge, float rightEdge, float topEdge, float bottomEdge)
    {
        this.LeftEdge = leftEdge;
        this.RightEdge = rightEdge;
        this.TopEdge = topEdge;
        this.BottomEdge = bottomEdge;
    }
}