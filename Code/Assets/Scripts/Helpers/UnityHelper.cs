﻿using UnityEngine;

/// <summary>
/// Helper methods related to the Unity in general
/// </summary>
public static class UnityHelper
{
    /// <summary>
    /// Pauses or unpauses the game via the TimeScale value
    /// </summary>
    /// <param name="isPaused">True == want game to be paused</param>
    public static void PauseGame(bool isPaused)
    {
        Time.timeScale = (isPaused ? 0.0f : 1.0f);
    }

    /// <summary>
    /// Is the game paused?
    /// </summary>
    /// <returns>Is the game paused?</returns>
    public static bool IsGamePaused()
    {
        return Time.timeScale == 0.0f;
    }

    /// <summary>
    /// Generates a Unity Color using human readible RGB, measured between 0 - 255 rather than 0 - 1
    /// </summary>
    /// <param name="r">Red</param>
    /// <param name="g">Green</param>
    /// <param name="b">Blue</param>
    /// <param name="a">Alpha</param>
    /// <returns>The Unity Color object</returns>
    public static Color GenerateUnityRGB(int r, int g, int b, int a = 255)
    {
        return new Color(r / 255.0f,
                         g / 255.0f,
                         b / 255.0f,
                         a / 255.0f);
    }
}