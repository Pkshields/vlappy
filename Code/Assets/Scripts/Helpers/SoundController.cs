﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Controller class to control the playing of music and sound effects in game.
/// Singleton, so add this to a GameObject at the root of your hierarchy, then define songs to be used in the inspector
/// </summary>
public class SoundController : MonoBehaviour
{
    /// <summary>
    /// The singleton instance of the SoundController
    /// </summary>
    private static SoundController instance;

    /// <summary>
    /// Gets the instanceof SoundController
    /// </summary>
    /// <value>
    /// The instance.
    /// </value>
    public static SoundController Instance
    {
        get { return instance; }
    }

    /// <summary>
    /// Singleton processing
    /// </summary>
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    /*********************************************************************************************/

    /// <summary>
    /// The raw list of sounds
    /// </summary>
    public AudioClip[] soundsList;

    /// <summary>
    /// The working processed sound list
    /// </summary>
    private Dictionary<string, AudioSource> workingSoundList = new Dictionary<string,AudioSource>();

    /// <summary>
    /// The current song playing
    /// </summary>
    private AudioSource currentSong;

    /// <summary>
    /// Initializes a sound as a song or sound effect
    /// </summary>
    /// <param name="soundName">Name of the sound to initialize</param>
    /// <param name="isSong">if set to <c>true</c> if you want to initialize a song</param>
    public void InitializeSound(string soundName, bool isSong)
    {
        InitializeAndGetSound(soundName, isSong);
    }

    /// <summary>
    /// Initializes and get a sound as a song or sound effect
    /// </summary>
    /// <param name="soundName">Name of the sound to initialize and get</param>
    /// <param name="isSong">if set to <c>true</c> if you want to initialize a song (optional if you are only getting)</param>
    /// <returns>The song processed</returns>
    /// <exception cref="UnityEngine.UnityException">Could not find a song with the provided name</exception>
    private AudioSource InitializeAndGetSound(string soundName, bool isSong = false)
    {
        AudioSource toPlay;
        if (!workingSoundList.TryGetValue(soundName, out toPlay))
        {
            AudioClip sound = FindSoundFromName(soundName);
            if (sound == null)
            {
                throw new UnityException("Could not find a song with the name " + soundName);
            }

            toPlay = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
            toPlay.clip = sound;
            toPlay.loop = isSong;
            workingSoundList.Add(soundName, toPlay);
        }

        return toPlay;
    }

    /// <summary>
    /// Plays a song
    /// </summary>
    /// <param name="songName">Name of the song to play</param>
    /// <exception cref="UnityEngine.UnityException">Could not find a song with the provided name</exception>
    public void PlaySong(string songName)
    {
        AudioSource toPlay = InitializeAndGetSound(songName, true);

        if (currentSong != null)
        {
            currentSong.Stop();
        }

        toPlay.Play();
        currentSong = toPlay;
    }

    /// <summary>
    /// Plays a sound effect.
    /// </summary>
    /// <param name="soundEffectName">Name of the sound effect to play</param>
    /// <exception cref="UnityEngine.UnityException">Could not find a song with the provided name</exception>
    public void PlaySoundEffect(string soundEffectName)
    {
        AudioSource toPlay = InitializeAndGetSound(soundEffectName, false);

        toPlay.Play();
    }

    /// <summary>
    /// Sets the volume of a song or sound effect
    /// </summary>
    /// <param name="sound">The sound to set</param>
    /// <param name="newVolume">The new volume</param>
    /// <exception cref="UnityEngine.UnityException">Sound with the provided name has not been initialized</exception>
    public void SetVolume(string sound, float newVolume)
    {
        AudioSource toPlay;
        if (!workingSoundList.TryGetValue(sound, out toPlay))
        {
            throw new UnityException("Sound with the name " + sound + " has not been initialized");
        }

        toPlay.volume = newVolume;
    }

    /// <summary>
    /// Gets the volume of a song or sound effect
    /// </summary>
    /// <param name="sound">The sound to get</param>
    /// <returns>The volume of said song</returns>
    /// <exception cref="UnityEngine.UnityException">Sound with the provided name has not been initialized</exception>
    public float GetVolume(string sound)
    {
        AudioSource toPlay;
        if (!workingSoundList.TryGetValue(sound, out toPlay))
        {
            throw new UnityException("Sound with the name " + sound + " has not been initialized");
        }

        return toPlay.volume;
    }

    /// <summary>
    /// Find a sound from the raw sound list
    /// </summary>
    /// <param name="name">The name of the sound to find</param>
    /// <returns>
    /// The found sound
    /// </returns>
    private AudioClip FindSoundFromName(string name)
    {
        foreach (var sound in soundsList)
        {
            if (sound.name == name)
            {
                return sound;
            }
        }

        return null;
    }

    public void MuteAll(bool toMute)
    {
        if (toMute)
        {
            AudioListener.volume = 0.0f;
        }
        else
        {
            AudioListener.volume = 1.0f;
        }
    }

    public bool IsMutedAll()
    {
        return AudioListener.volume == 0.0f;
    }
}