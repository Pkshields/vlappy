﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generic script for something hitting a trigger point and moving said object to another designated position
/// </summary>
public class ResetScript : MonoBehaviour
{
    /// <summary>
    /// Gets the new spawn location.
    /// </summary>
    /// <returns>The new spawn location</returns>
    public Vector3 GetSpawnLocation()
    {
        Transform spawnLocation = this.transform.GetChild(0);
        return spawnLocation.position;
    }
}
