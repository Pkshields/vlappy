﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Manager managing the players score (and deaths)
/// </summary>
public class SaveManager
{
    /// <summary>
    /// Adds the score to the current scores
    /// </summary>
    /// <param name="score">Score to add</param>
    public static void AddScore(int score)
    {
        int topScore = GetTopScore();
        if (score > topScore)
        {
            PlayerPrefs.SetInt("PlayerTopScore", score);
        }

        PlayerPrefs.SetInt("PlayerScore", score);
    }

    /// <summary>
    /// Gets the last score obtained
    /// </summary>
    /// <returns>The last player score</returns>
    public static int GetLastScore()
    {
        return PlayerPrefs.GetInt("PlayerScore", 0);
    }

    /// <summary>
    /// Gets the top score obtained
    /// </summary>
    /// <returns>The top player score</returns>
    public static int GetTopScore()
    {
        return PlayerPrefs.GetInt("PlayerTopScore", 0);
    }

    /// <summary>
    /// Increments the number of player deaths
    /// </summary>
    public static void IncrementDeaths()
    {
        int topScore = GetDeaths();
        PlayerPrefs.SetInt("PlayerDeaths", topScore + 1);
    }

    /// <summary>
    /// Gets the number of player deaths
    /// </summary>
    /// <returns>The number of player deaths</returns>
    public static int GetDeaths()
    {
        return PlayerPrefs.GetInt("PlayerDeaths", 0);
    }

    /// <summary>
    /// Sets the players gravity option
    /// </summary>
    /// <param name="isVVVVVV">if set to <c>true</c> [is VVVVVV].</param>
    public static void SetGravity(bool isVVVVVV)
    {
        PlayerPrefs.SetInt("PlayerGravity", (isVVVVVV ? 1 : 0));
    }

    /// <summary>
    /// Gets the players gravity option
    /// </summary>
    /// <returns>If we are using VVVVVV gravity or real gravity</returns>
    public static bool GetGravity()
    {
        return PlayerPrefs.GetInt("PlayerGravity", 1) == 1;
    }
}