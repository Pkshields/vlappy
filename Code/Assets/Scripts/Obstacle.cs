﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Script for the full obstacle object
/// </summary>
public class Obstacle : MonoBehaviour
{
    /// <summary>
    /// The distance between each object
    /// </summary>
    private static float DistanceBetween = 3.0f;

    /// <summary>
    /// The minimum height gap between the two obstacles
    /// </summary>
	private static float MinHeightGap = 2.0f;

    /// <summary>
    /// The maximum height gap between the two obstacles
    /// </summary>
	private static float MaxHeightGap = 5.5f;

    /// <summary>
    /// Dimensions of the 2D camera
    /// </summary>
	private Vector2 cameraDimensions;

    /// <summary>
    /// The Obstale Parts attached to this complete Obstacle thing
    /// </summary>
	private ObstaclePart[] obstacleChildren;

    /// <summary>
    /// Access the the PointsUI script
    /// </summary>
    private PointsText pointsUI;

    /// <summary>
    /// Starts this instance.
    /// </summary>
	private void Start()
    {
        obstacleChildren = this.gameObject.GetComponentsInChildren<ObstaclePart>();
        foreach (var obstacleChild in obstacleChildren)
        {
            obstacleChild.StartManual();
        }

        cameraDimensions = CameraHelper.GetCameraDimensions();

        SetObstaclesYPosition(GetRandomYPosition());

        pointsUI = GameObject.FindGameObjectWithTag(GameTags.UI).GetComponentInChildren<PointsText>();

        SetColorPalette();
	}
	
    /// <summary>
    /// Updates this instance.
    /// </summary>
	private void Update()
    {
	}

    /// <summary>
    /// Handle trigger collision resolution (on enter) for this full OBstacle object, not the separate parts
    /// </summary>
    /// <param name="collision">Object collided with</param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == GameTags.ObstacleResetTrigger)
        {
            ResetScript obstacleResetTrigger = collision.gameObject.GetComponent<ResetScript>();
            Vector3 spawnPosition = obstacleResetTrigger.GetSpawnLocation();
            Reinitialize(spawnPosition);
        }
    }

    /// <summary>
    /// Handle trigger collision resolution (on exit) for this full Obstacle object, not the separate parts
    /// </summary>
    /// <param name="collision">Object collided with</param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == GameTags.VlappyMan)
        {
            pointsUI.Increment();
        }
    }

    /*************************************************************************************************/

    /// <summary>
    /// Reinitializes the full Obstacle object
    /// </summary>
    /// <param name="spawnPosition">The spawn position</param>
    private void Reinitialize(Vector3 spawnPosition)
    {
        Transform transform = rigidbody2D.transform;
        transform.position = spawnPosition;

        SetObstaclesYPosition(GetRandomYPosition());
    }

    /// <summary>
    /// Gets a random y position to spawn at
    /// </summary>
    /// <returns></returns>
    private float GetRandomYPosition()
    {
        return Random.Range(MinHeightGap, MaxHeightGap);
    }

    /// <summary>
    /// Sets the obstacles y position (for each ObstaclePart)
    /// </summary>
    /// <param name="yPosition">The new Y position</param>
    private void SetObstaclesYPosition(float yPosition)
    {
        foreach(var obstacleChild in obstacleChildren)
        {
            obstacleChild.SetYPosition(yPosition);
        }
    }

    /// <summary>
    /// Sets the color pallette for the children
    /// </summary>
    public void SetColorPalette()
    {
        Color currentColor = ColorPalette.GetCurrentSecondaryColor();
        foreach (var child in obstacleChildren)
        {
            child.SetColor(currentColor);
        }
    }
}
