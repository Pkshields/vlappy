﻿using UnityEngine;

/// <summary>
/// Script to generate and access random (predetermined list of) color palettes
/// </summary>
public class ColorPalette : MonoBehaviour
{
    /// <summary>
    /// The current color palette
    /// </summary>
    private static ColorPaletteData currentPalette;
    /// <summary>
    /// The current color palette number
    /// </summary>
    private static int currentPaletteNum;

    /// <summary>
    /// Awakes this instance.
    /// </summary>
    private void Awake()
    {
        GenerateRandomPalette();
    }

    /// <summary>
    /// Generates the next random palette.
    /// </summary>
    public static void GenerateRandomPalette()
    {
        int random = -1;
        do
        {
            random = Random.Range(0, ColorPaletteList.Count - 1);
        } while (random == currentPaletteNum);

        currentPaletteNum = random;
        currentPalette = ColorPaletteList.List[random];
    }

    /// <summary>
    /// Get the current color palette.
    /// </summary>
    /// <returns></returns>
    public static ColorPaletteData GetCurrentPalette()
    {
        return currentPalette;
    }

    /// <summary>
    /// Gets the current primary color
    /// </summary>
    /// <returns>The current primary color</returns>
    public static Color GetCurrentPrimaryColor()
    {
        return currentPalette.PrimaryColor;
    }

    /// <summary>
    /// Gets the current secondary color
    /// </summary>
    /// <returns>The current secondary color</returns>
    public static Color GetCurrentSecondaryColor()
    {
        return currentPalette.SecondaryColor;
    }
}